from flask import Flask, render_template, url_for, redirect, session, request
import json
from flask_googlelogin import GoogleLogin
from flask.ext.sqlalchemy import SQLAlchemy
from datetime import datetime
from functools import wraps


app = Flask(__name__)
app.config['GOOGLE_LOGIN_CLIENT_ID'] = "435486204392-7lovekrenmopsae7qonlp8orgfl2r7g8.apps.googleusercontent.com"
app.config['GOOGLE_LOGIN_CLIENT_SECRET'] = "Sh_HOG-SY08EfTFY2SHPXAef"
app.config['GOOGLE_LOGIN_REDIRECT_URI'] = "http://localhost:5000/oauth2callback"
app.secret_key = 'dsgdgey[]gsdgyye;fhhreyergd[f'


googlelogin = GoogleLogin(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.db'
db = SQLAlchemy(app)

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not "email" in session:
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return decorated_function


@app.route('/oauth2callback')
@googlelogin.oauth2callback
def create_or_update_user(token, userinfo, **params):
    print "==============" + userinfo['id']
    user = User.query.filter_by(google_id=userinfo['id']).first()
    print str(user)
    if user:
        user.name = userinfo['name']
        user.avatar = userinfo['picture']
    else:
        user = User(google_id=userinfo['id'],
                    name=userinfo['name'],
                    avatar=userinfo['picture'],
                    email=userinfo['email'])
    db.session.add(user)
    db.session.commit()
    db.session.flush()
    login_user(user)
    return redirect(url_for('index'))

def login_user(user):
	session["name"] = user.name
	session["email"] = user.email
	session["avatar"] = user.avatar
	session["userid"] = user.id
	session["logged_in"] = True


@app.route("/login")
def login():
	return redirect(googlelogin.login_url(approval_prompt='force',scopes=["email"]))

@app.route("/")
def index():
	return render_template("index.html")


# @app.route("/<profile>")
# def profile_def(profile):
# 	return render_template("profile.html", profile=profile)

@app.route("/logout")
def logout():
	session.clear()
	return redirect(url_for('index'))


# APP ROUTES
@app.route("/eisenmatrix")
@login_required
def matrix():
	return render_template("eisen.html")

@app.route("/task", methods=["GET","POST","PUT","DELETE"])
@login_required
def task():
	if request.method == "POST":

		task = request.form.get("task")
		quarter = request.form.get("quarter")
		status = 0
		user = session["userid"]
		task = Task(task, quarter,status,user)
		db.session.add(task)
		db.session.commit()
		db.session.flush()
		return json.dumps({"status":"OK"})

	if request.method == "GET":
		taskid = request.form.get("taskid")
		
		task = Task.query.filter_by(id=taskid, user=session["userid"]).first()
		if task and task.user:
			return json.dumps(task.as_dict())
		else:
			return json.dumps({})
	if request.method == "PUT":
		taskid = request.form.get("taskid")
		task = request.form.get("task")
		quarter = request.form.get("quarter")
		status = request.form.get("status")
		t = Task.filter_by(id=taskid, user=session["userid"]).first()
		if t:
		    t.task = task
		    t.quarter = quarter
		    t.status = status
		    db.session.commit()
		    return json.dumps(Task.query.filter_by(id=taskid,user=session["userid"]).first().as_dict())
		else:
			#TODO : sane error handling
			return json.dumps({"staus":"error"})
		
	if request.method == "DELETE":
		taskid = request.form.get("taskid")
		t = Task.query.filter_by(id=taskid, user=session["userid"]).first()
		if t:
		    db.session.delete(t)
		    db.session.commit()
		return json.dumps({"status":"OK"})

@app.route("/task/quarter/<q>")
@login_required
def task_by_q(q):
	tasks = Task.query.filter_by(user=session["userid"], status=0, quarter=q)
	all_tasks = []
	for t in tasks:
		all_tasks.append(t.as_dict())
	return json.dumps(all_tasks)

@app.route("/task/quarter", methods=["POST"])
@login_required
def update_quarter():
	taskid = request.form.get("taskid")
	quarter = request.form.get("quarter")
	task = Task.query.filter_by(id = taskid, user = session["userid"]).first()
	print task.as_dict()
	print taskid, quarter
	if task :
		task.quarter = quarter
		db.session.commit()
		task = Task.query.filter_by(id = taskid, user = session["userid"]).first()
		return json.dumps(task.as_dict())
	else:
		return json.dumps({"status":"error"})

@app.route("/task/status", methods=["POST"])
@login_required
def update_status():
	taskid = request.form.get("taskid")
	status = request.form.get("status")
	task = Task.query.filter_by(id = taskid, user = session["userid"]).first()
	print taskid, status
	if task :
		task.status = status
		db.session.commit()
		return json.dumps(task.as_dict())
	else:
		return json.dumps({"status":"error"})




# END APP ROUTES



# Tasks app

@app.route("/tasks")
@login_required
def task_page():
	return render_template("tasks.html")

# End Tasks app



# Models
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    email = db.Column(db.String(120), unique=True)
    google_id = db.Column(db.String(250), unique=True)
    avatar = db.Column(db.String(250), unique=True)
    joined_date = db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, name, google_id, email, avatar):
        self.name = name
        self.email = email
        self.avatar = avatar
        self.google_id = google_id

    def __repr__(self):
        return '<User %r>' % self.name

class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.Column(db.String(140))
    quarter = db.Column(db.Integer)
    status = db.Column(db.Integer)
    added_date = db.Column(db.DateTime, default=datetime.utcnow)
    user = db.Column(db.Integer,db.ForeignKey('user.id'))

    def __init__(self, task, quarter, status, user):
        self.task = task
        self.quarter = quarter
        self.status = status
        self.user = user

    def __repr__(self):
        return '<User %r>' % self.task

    def as_dict(self):
    	return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}
# End Models

if __name__ == '__main__':
	import os
	if not os.path.exists("/tmp/test.db"):
		db.create_all()
	app.run(debug=True)

